package com.dotlab.jpls.academia.Authentication;

import com.dotlab.jpls.academia.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JP on 24/03/2017.
 */



public  class Authentication {

    private List<User> users = new ArrayList<User>();

    public Authentication() {

        User user1 = new User("JP","983098745","joaopaulo_jpls@hotmail.com","12345");

        users.add(user1);

    }

    public boolean addNewUser(User user){
        return users.add(user);
    }



    public User validateUser(String name, String password){

        for (User user : users){

            if (name.equals(user.getName()) && password.equals(user.getPassword())){

                return user;

            }

        }
        return null;

    }

}
