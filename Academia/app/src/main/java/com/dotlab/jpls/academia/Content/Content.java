package com.dotlab.jpls.academia.Content;

import com.dotlab.jpls.academia.Enuns.DaysWeek;
import com.dotlab.jpls.academia.model.Training;

/**
 * Created by JP on 25/03/2017.
 */

public class Content {


    public static final Training[] trainings = { new Training(DaysWeek.Sunday,"-","Descanso"),
                                    new Training(DaysWeek.Monday,"Braço","Curl com Halteres"),
                                    new Training(DaysWeek.Tuesday,"Perna","Agachamento"),
                                    new Training(DaysWeek.Wednesday,"Peito","Supino Reto"),
                                    new Training(DaysWeek.Thursday,"Braço","Supino Fechado"),
                                    new Training(DaysWeek.Friday,"Abdômen","Abdominal"),
                                    new Training(DaysWeek.Saturday,"-","Descanso")};


    public static final Training trainingByDay(DaysWeek day){

        for (Training t: trainings) {
            if( t.getDay() == day){
                return t;
            }

        }
        return null;
    }
}
