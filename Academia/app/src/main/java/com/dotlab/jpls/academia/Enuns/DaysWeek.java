package com.dotlab.jpls.academia.Enuns;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by JP on 24/03/2017.
 */

public enum DaysWeek {

    Sunday("Domingo"),
    Monday("Segunda"),
    Tuesday("Terça"),
    Wednesday("Quarta"),
    Thursday("Quinta"),
    Friday("Sexta"),
    Saturday("Sábado");

    private String name;

    public String getName() {
        return name;
    }

    DaysWeek(String name) {

        this.name = name;

    }

    public static List<String> getNames() {

        List<String> days = new ArrayList();

        for (DaysWeek day :DaysWeek.values()) {
            days.add(day.getName());
        }

        return days;

    }

}
