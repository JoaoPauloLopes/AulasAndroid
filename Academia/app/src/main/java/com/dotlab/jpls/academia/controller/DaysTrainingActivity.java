package com.dotlab.jpls.academia.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dotlab.jpls.academia.Content.Content;
import com.dotlab.jpls.academia.Enuns.DaysWeek;
import com.dotlab.jpls.academia.R;

public class DaysTrainingActivity extends Activity implements AdapterView.OnItemClickListener{

    private ListView daysList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_days_training);

        daysList = (ListView) findViewById(R.id.list_view_days);
        ArrayAdapter adpter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,DaysWeek.getNames());
        daysList.setAdapter(adpter);
        daysList.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        Intent it = new Intent(this,TrainingDayActivity.class);
        it.putExtra("training",Content.trainingByDay(DaysWeek.values()[position]));
        startActivity(it);

    }
}
