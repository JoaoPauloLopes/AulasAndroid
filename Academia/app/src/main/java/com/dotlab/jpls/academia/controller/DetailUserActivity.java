package com.dotlab.jpls.academia.controller;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dotlab.jpls.academia.R;
import com.dotlab.jpls.academia.model.User;

public class DetailUserActivity extends Activity implements View.OnClickListener{

    private TextView txtViewName;
    private TextView txtViewPhone;
    private TextView txtViewEmail;
    private Button btnShare;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);


        txtViewName = (TextView) findViewById(R.id.txt_view_value_name);
        txtViewPhone = (TextView) findViewById(R.id.txt_view_value_phone);
        txtViewEmail = (TextView) findViewById(R.id.txt_view_value_email);

        btnShare = (Button) findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);

        user = getIntent().getExtras().getParcelable("user");
        if(user != null){

            txtViewName.setText(user.getName());
            txtViewPhone.setText(user.getPhone());
            txtViewEmail.setText(user.getEmail());

        }
        else{
            txtViewName.setText("?");
            txtViewPhone.setText("?");
            txtViewEmail.setText("?");

        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == btnShare.getId()){

            Intent it = new Intent();
            it.setAction(it.ACTION_SEND);
            it.putExtra(it.EXTRA_TITLE,"Usuário");
            it.putExtra(it.EXTRA_TEXT,user.toString());
            it.putExtra(it.EXTRA_EMAIL,user.getEmail());
            it.setType("text/plain");
            startActivity(it);

        }


    }
}
