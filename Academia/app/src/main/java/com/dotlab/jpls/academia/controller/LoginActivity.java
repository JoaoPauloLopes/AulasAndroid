package com.dotlab.jpls.academia.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dotlab.jpls.academia.Authentication.Authentication;
import com.dotlab.jpls.academia.R;
import com.dotlab.jpls.academia.model.User;

public class LoginActivity extends Activity implements View.OnClickListener{

    private EditText user;
    private EditText password;
    private Button enter;
    private Authentication aut = new Authentication();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = (EditText) findViewById(R.id.edit_txt_user);
        password = (EditText) findViewById(R.id.edit_txt_password);
        enter = (Button) findViewById(R.id.btn_enter);
        enter.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if (v.getId() == enter.getId()){

            if (user.getText().toString().isEmpty() || password.getText().toString().isEmpty()){

                showAlertMsg("Campos Vazios","Preencha todos os campos");
            }
            else{
                User userAut = aut.validateUser(this.user.getText().toString(),this.password.getText().toString());
                if (userAut == null){

                    showAlertMsg("Dados Incorretos","Usuário ou senha inválidos");
                }
                else{

                    Intent it = new Intent(this,MenuActivity.class);
                    it.putExtra("user",userAut);
                    startActivity(it);


                }

            }

        }

    }


    private void checkData(){



    }

    private  void showAlertMsg(String title, String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }



}
