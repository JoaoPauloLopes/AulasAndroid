package com.dotlab.jpls.academia.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dotlab.jpls.academia.R;
import com.dotlab.jpls.academia.model.User;


public class MenuActivity extends Activity implements View.OnClickListener{

    private TextView txtViewUserName;
    private Button btnPersonalData;
    private Button btnDaysTraining;
    private Button btnCreateTraining;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        txtViewUserName = (TextView) findViewById(R.id.txt_view_name_user);
        user = getIntent().getExtras().getParcelable("user");
        if(user != null){
            txtViewUserName.setText("Usuário Online: "+user.getName());
        }
        else{
            txtViewUserName.setText("Usuário Online: ???");
        }

        btnPersonalData = (Button) findViewById(R.id.btn_user_data);
        btnPersonalData.setOnClickListener(this);

        btnDaysTraining = (Button) findViewById(R.id.btn_days_training);
        btnDaysTraining.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


        Class<?> destiny;

        switch (v.getId()){
            case R.id.btn_user_data:
                destiny = DetailUserActivity.class;
                break;
            case R.id.btn_days_training:
                destiny = DaysTrainingActivity.class;
                break;
            default:
                destiny = MenuActivity.class;

        }

        Intent it = new Intent(this,destiny);
        it.putExtra("user",user);
        startActivity(it);


    }
}
