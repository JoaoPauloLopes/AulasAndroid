package com.dotlab.jpls.academia.controller;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.dotlab.jpls.academia.R;
import com.dotlab.jpls.academia.model.Training;

public class TrainingDayActivity extends Activity {

    private TextView day;
    private TextView member;
    private TextView training;
    private Training trainingDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_day);

        trainingDay = getIntent().getExtras().getParcelable("training");

        day = (TextView) findViewById(R.id.txt_view_day_value);
        member = (TextView) findViewById(R.id.txt_view_value_member);
        training = (TextView) findViewById(R.id.txt_view_value_training);

        day.setText(trainingDay.getDay().getName());
        member.setText(trainingDay.getMember());
        training.setText(trainingDay.getTraining());



    }
}
