package com.dotlab.jpls.academia.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dotlab.jpls.academia.Enuns.DaysWeek;

/**
 * Created by JP on 24/03/2017.
 */

public class Training implements Parcelable{

    private DaysWeek day;
    private String member;
    private String training;

    public DaysWeek getDay() {
        return day;
    }

    public void setDay(DaysWeek day) {
        this.day = day;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getTraining() {
        return training;
    }

    public void setTraining(String training) {
        this.training = training;
    }

    public Training(DaysWeek day, String member, String training) {

        this.day = day;
        this.member = member;
        this.training = training;
    }

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(member);
        dest.writeString(training);
        dest.writeString(day.name());

    }

    Training(Parcel p){

        member = p.readString();
        training = p.readString();
        day = DaysWeek.valueOf(p.readString());

    }

    public static final Parcelable.Creator<Training> CREATOR = new Parcelable.Creator<Training>(){
        public Training createFromParcel(Parcel in){

            return new Training(in);
        }

        public Training[] newArray(int size){
            return new Training[size];
        }


    };



}
