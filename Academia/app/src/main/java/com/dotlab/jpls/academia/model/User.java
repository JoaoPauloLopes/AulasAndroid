package com.dotlab.jpls.academia.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JP on 24/03/2017.
 */

public class User implements Parcelable{

    private String name;
    private String phone;
    private String email;
    private String password;



    public User(String name, String phone, String email, String password) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return  "Name: " + name + "\n" +
                "Phone: " + phone + "\n" +
                "Email: " + email ;
    }

    private User(Parcel p){
        name = p.readString();
        phone = p.readString();
        email = p.readString();
        password = p.readString();
    }
    public static final Parcelable.Creator<User>
            CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(password);
    }
}
