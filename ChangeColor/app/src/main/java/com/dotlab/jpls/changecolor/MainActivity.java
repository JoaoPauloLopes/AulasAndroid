package com.dotlab.jpls.changecolor;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends Activity implements SeekBar.OnSeekBarChangeListener{

    private Switch aSwitchActiveSeekBarR;
    private Switch aSwitchActiveSeekBarG;
    private Switch aSwitchActiveSeekBarB;


    private SeekBar seekBarChangeColorR;
    private SeekBar seekBarChangeColorG;
    private SeekBar seekBarChangeColorB;
    private TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aSwitchActiveSeekBarR = (Switch) findViewById(R.id.id_switch_R);
        aSwitchActiveSeekBarG = (Switch) findViewById(R.id.id_switch_G);
        aSwitchActiveSeekBarB = (Switch) findViewById(R.id.id_switch_B);
        settingSwitch();

        seekBarChangeColorR = (SeekBar) findViewById(R.id.id_seekbar_R);
        seekBarChangeColorR.setOnSeekBarChangeListener(this);

        seekBarChangeColorG = (SeekBar) findViewById(R.id.id_seekbar_G);
        seekBarChangeColorG.setOnSeekBarChangeListener(this);

        seekBarChangeColorB = (SeekBar) findViewById(R.id.id_seekbar_B);
        seekBarChangeColorB.setOnSeekBarChangeListener(this);

        textView = (TextView) findViewById(R.id.id_txt_view);
        textView.setText("FFFFFF");

    }

    private void settingSwitch(){

        aSwitchActiveSeekBarR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    seekBarChangeColorR.setEnabled(false);

                }
                else{

                    seekBarChangeColorR.setEnabled(true);

                }

            }
        });

        aSwitchActiveSeekBarG.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    seekBarChangeColorG.setEnabled(false);


                }
                else{

                    seekBarChangeColorG.setEnabled(true);

                }

            }
        });

        aSwitchActiveSeekBarB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    seekBarChangeColorB.setEnabled(false);

                }
                else{

                    seekBarChangeColorB.setEnabled(true);

                }

            }
        });

    }



    private void updateBackground()
    {
        int seekR = seekBarChangeColorR.getProgress();
        int seekG = seekBarChangeColorG.getProgress();
        int seekB = seekBarChangeColorB.getProgress();

        int color = 0xff000000
                + seekR * 0x10000
                + seekG * 0x100
                + seekB;

        LinearLayout lLayout = (LinearLayout) findViewById(R.id.activity_main);

        lLayout.setBackgroundColor(color);

        String hexColor = Integer.toHexString(color).toUpperCase();

        textView.setText(hexColor.substring(2));

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        updateBackground();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        updateBackground();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        updateBackground();
    }
}
