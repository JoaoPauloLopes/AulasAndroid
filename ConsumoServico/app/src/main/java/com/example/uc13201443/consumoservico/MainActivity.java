package com.example.uc13201443.consumoservico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends Activity implements View.OnClickListener{

    private static final String URL = "https://viacep.com.br/ws/72135240/json/";
    TextView result;
    EditText cep;
    Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = (TextView) findViewById(R.id.id_txt_view_result);
        cep = (EditText) findViewById(R.id.id_edit_text_cep) ;
        search = (Button) findViewById(R.id.id_btn_search);
        search.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        GetAddress getAddress = new GetAddress();
        getAddress.execute(cep.getText().toString());


    }


    public class GetAddress extends AsyncTask <String,Void,String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(MainActivity.this,"Aguarde","Buscando CEP");

        }

        @Override
        protected String doInBackground(String... params) {

            try {

                JSONObject address = new JSONObject(Network.getAddressByCEP(params[0]));

                return "UF: "+address.getString("uf")+
                        "\nCidade: "+address.getString("bairro")+
                        "\nEndereço: " +address.getString("logradouro");


            }catch (Exception e){

                return null;
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            result.setText(s);

        }
    }
}
