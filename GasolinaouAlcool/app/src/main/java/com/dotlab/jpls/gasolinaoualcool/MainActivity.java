package com.dotlab.jpls.gasolinaoualcool;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    private EditText editTxt_gasoline;
    private EditText editTxt_alcohol;
    private Button btn_calculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTxt_gasoline = (EditText) findViewById(R.id.editTxtGasoline);
        editTxt_alcohol = (EditText) findViewById(R.id.editTxtAlcohol);
        btn_calculate = (Button) findViewById(R.id.btn_calculate);
        btn_calculate.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if (v.getId() == btn_calculate.getId()) {

            if(!editTxt_gasoline.getText().toString().isEmpty() && !editTxt_alcohol.getText().toString().isEmpty()) {

                if(stringIsNumber(editTxt_gasoline.getText().toString()) && stringIsNumber(editTxt_alcohol.getText().toString())){

                    Float valueGasoline = Float.parseFloat(editTxt_gasoline.getText().toString());
                    Float valueAlcohol = Float.parseFloat(editTxt_alcohol.getText().toString());

                    if (advantageousFuel(valueGasoline,valueAlcohol) == Fuel.Alcohol) {

                        showAlertMsg("Álcool", "Comparando os dois preços é mais vantajoso abastecer com Álcool");

                    } else {

                        showAlertMsg("Gasolina", "Comparando os dois preços é masi vantajoso abastecer com Gasolina");

                    }
                }
                else{

                    showAlertMsg("Aviso", "Você sabe que é pra colocar números, não se faça de besta!");

                }
            }
            else{

                showAlertMsg("Aviso", "Preencha todos os campos!");
            }

        }

    }

    private Fuel advantageousFuel(Float valueGasoline, Float valueAlcohol){

        if ((valueAlcohol / valueGasoline) < 0.7) {

            return Fuel.Alcohol;
        }
        else{

            return Fuel.Gasoline;

        }

    }


    private boolean stringIsNumber(String s){

        try{
            Float.parseFloat(s);
            return true;
        }catch(Exception e){
            return false;
        }

    }


    private void showAlertMsg(String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }
}
