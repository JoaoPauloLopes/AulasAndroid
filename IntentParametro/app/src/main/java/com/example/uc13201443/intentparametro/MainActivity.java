package com.example.uc13201443.intentparametro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/*
*
* Exemplo de Passagem de parametro objeto pela intent
*
* */

public class MainActivity extends Activity implements View.OnClickListener{


    private Button secondActivity;
    private EditText name;
    private EditText university;
    private EditText course;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        secondActivity = (Button) findViewById(R.id.btn_second_activity);
        secondActivity.setOnClickListener(this);

        name = (EditText) findViewById(R.id.edit_text_name);
        university = (EditText) findViewById(R.id.edit_text_university);
        course = (EditText) findViewById(R.id.edit_text_course);

        name.setHint("Name");
        university.setHint("University");
        course.setHint("Course");


    }

    @Override
    public void onClick(View v) {

        student = new Student(name.getText().toString(),university.getText().toString(),course.getText().toString());

        Intent it = new Intent(this,SecondActivity.class);
        //passagem de objeto como parametro (Objeto serializado)
        it.putExtra("student",student);

//        it.putExtra("name",name.getText().toString());
//        it.putExtra("university",university.getText().toString());
//        it.putExtra("course",course.getText().toString());

        startActivity(it);

    }
}
