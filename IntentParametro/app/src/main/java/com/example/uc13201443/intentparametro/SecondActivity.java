package com.example.uc13201443.intentparametro;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

    private TextView name;
    private TextView university;
    private TextView course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        name = (TextView) findViewById(R.id.text_view_name);
        university = (TextView) findViewById(R.id.text_view_university);
        course = (TextView) findViewById(R.id.text_view_course);

        Student student = getIntent().getExtras().getParcelable("student");

        if(student != null){

            name.setText("Name: "+student.name);
            university.setText("University: "+student.university);
            course.setText("Couser: "+student.course);

        }


//        name.setText("Name: "+getIntent().getStringExtra("name"));
//        university.setText("University: "+getIntent().getStringExtra("university"));
//        course.setText("Couser: "+getIntent().getStringExtra("course"));


    }
}
