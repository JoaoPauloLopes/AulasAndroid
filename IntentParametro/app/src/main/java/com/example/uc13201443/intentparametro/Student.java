package com.example.uc13201443.intentparametro;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by UC13201443 on 15/03/2017.
 */

public class Student implements Parcelable {

    public String name;
    public String university;
    public String course;

    public Student(String name,String university,String course) {
        this.name = name;
        this.university = university;
        this.course = course;
    }

    // Implementação Parcelable para serializar objeto
    private Student(Parcel p){
        name = p.readString();
        university = p.readString();
        course = p.readString();
    }

    public static final Parcelable.Creator<Student>
            CREATOR = new Parcelable.Creator<Student>() {

        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeString(university);
        dest.writeString(course);

    }
}
