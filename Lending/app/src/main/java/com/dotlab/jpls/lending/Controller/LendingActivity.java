package com.dotlab.jpls.lending.Controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dotlab.jpls.lending.DAO.BasicDAO;
import com.dotlab.jpls.lending.DAO.ItemDAO;
import com.dotlab.jpls.lending.Model.Address;
import com.dotlab.jpls.lending.Model.Item;
import com.dotlab.jpls.lending.Network.Network;
import com.dotlab.jpls.lending.R;

import org.json.JSONObject;

public class LendingActivity extends Activity implements View.OnClickListener{

    private Button btnCharge;
    private Button btnRefund;
    private Button btnLend;
    private Button btnCep;

    private EditText editTextName;
    private EditText editTextDateLending;
    private EditText editTextDateDevolution;
    private EditText editTextNamePersonLending;
    private EditText editTextCep;

    private ItemDAO itemDAO;
    private Item item;

    private int state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lending);

        btnCharge = (Button) findViewById(R.id.id_btn_charge);
        btnCharge.setOnClickListener(this);

        btnRefund = (Button) findViewById(R.id.id_btn_refund);
        btnRefund.setOnClickListener(this);

        btnLend = (Button) findViewById(R.id.id_btn_lend_item);
        btnLend.setOnClickListener(this);

        btnCep = (Button) findViewById(R.id.id_btn_cep);
        btnCep.setOnClickListener(this);

        editTextName = (EditText) findViewById(R.id.id_edit_txt_name);
        editTextDateLending = (EditText) findViewById(R.id.id_edit_txt_date_lending);
        editTextDateDevolution = (EditText) findViewById(R.id.id_edit_txt_date_devolution);
        editTextNamePersonLending = (EditText) findViewById(R.id.id_edit_txt_lending_person);
        editTextCep = (EditText) findViewById(R.id.id_edit_txt_cep);

        itemDAO = new ItemDAO(this);

        Intent intent = getIntent();

        if(intent.hasExtra("item")){

            state = 1;

            item = (Item) intent.getExtras().getSerializable("item");

            editTextName.setText(item.getName());
            editTextDateLending.setText(item.getDateLending());
            editTextDateDevolution.setText(item.getDateDevolution());
            editTextNamePersonLending.setText(item.getNamePersonLending());
            editTextCep.setText(item.getCep());

            btnLend.setText("Atualizar");
            btnLend.setBackgroundColor(getResources().getColor(R.color.Azul));
            btnCharge.setVisibility(View.VISIBLE);
            btnRefund.setVisibility(View.VISIBLE);
            btnCep.setVisibility(View.VISIBLE);

        }
        else{

            state = 2;

            btnLend.setBackgroundColor(getResources().getColor(R.color.Verde));
            btnCharge.setVisibility(View.GONE);
            btnRefund.setVisibility(View.GONE);
            btnCep.setVisibility(View.GONE);

        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.id_btn_charge:
                shared();
                break;

            case R.id.id_btn_refund:
                refundItem();
                break;

            case R.id.id_btn_lend_item:
                if(state == 1){
                    updateItem();
                }
                else{
                    lendingItem();
                }

                break;
            case R.id.id_btn_cep:
                GetAddress getAddress = new GetAddress();
                getAddress.execute(editTextCep.getText().toString());
                break;
        }

    }

    public void updateItem(){

        Item i = new Item(item.getId(),editTextName.getText().toString(),
                editTextDateLending.getText().toString(),
                editTextDateDevolution.getText().toString(),
                editTextNamePersonLending.getText().toString(),
                editTextCep.getText().toString());

        if (itemDAO.edit(i)){

            Toast.makeText(this, "Item atualizado!",
                    Toast.LENGTH_LONG).show();

        }
        else {

            Toast.makeText(this, "Erro ao atualizar item!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void shared(){
        Intent it = new Intent();
        it.setAction(it.ACTION_SEND);
        it.putExtra(it.EXTRA_TEXT,item.toString());
        it.setType("text/plain");
        startActivity(it);
    }

    public void refundItem(){


        if(itemDAO.delete(item)){

            showAlertMsg("Item","O item foi devolvido!");
        }
        else{

            Toast.makeText(this, "Erro ao tentar excluir o item da lista!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void lendingItem(){

        if(!isEmptyFields()){

            Item item = new Item(editTextName.getText().toString(),
                    editTextDateLending.getText().toString(),
                    editTextDateDevolution.getText().toString(),
                    editTextNamePersonLending.getText().toString(),
                    editTextCep.getText().toString());

            if(itemDAO.insert(item)){

                Toast.makeText(this, "Dados salvos!",
                        Toast.LENGTH_LONG).show();

                clearFields();


            }
            else{

                Toast.makeText(this, "Erro ao tentar salvar dados",
                        Toast.LENGTH_LONG).show();

            }


        }
        else{

            Toast.makeText(this, "Preecha todos os campos!",
                    Toast.LENGTH_LONG).show();

        }

    }

    public void clearFields(){
        editTextName.setText("");
        editTextDateLending.setText("");
        editTextDateDevolution.setText("");
        editTextNamePersonLending.setText("");
        editTextCep.setText("");
    }

    public boolean isEmptyFields(){

        return editTextName.getText().toString().isEmpty() ||
                editTextDateLending.getText().toString().isEmpty() ||
                editTextDateDevolution.getText().toString().isEmpty() ||
                editTextNamePersonLending.getText().toString().isEmpty() ||
                editTextCep.getText().toString().isEmpty();


    }

    private void showAlertMsg(String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }

    public class GetAddress extends AsyncTask<String,Void,Address> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(LendingActivity.this,"Aguarde","Buscando CEP");

        }

        @Override
        protected Address doInBackground(String... params) {

            try {

                JSONObject addressJson = new JSONObject(Network.getAddressByCEP(params[0]));

                return new Address(addressJson);


            }catch (Exception e){

                return null;
            }


        }

        @Override
        protected void onPostExecute(Address address) {
            super.onPostExecute(address);
            progressDialog.dismiss();
            if(address != null) {
                showAlertMsg("Endereço", address.toString());

            }
            else {
                showAlertMsg("Erro", "Ocorreu um erro na requisição!");
            }
        }
    }




}
