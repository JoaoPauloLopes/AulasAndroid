package com.dotlab.jpls.lending.Controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.dotlab.jpls.lending.DAO.ItemDAO;
import com.dotlab.jpls.lending.Model.Item;
import com.dotlab.jpls.lending.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener,AdapterView.OnItemClickListener{

    private ListView listViewLending;
    private Button btnLend;

    private ItemDAO itemDAO;
    private List<Item> itens;
    private ArrayList<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewLending = (ListView) findViewById(R.id.id_list_view_lending);

        btnLend = (Button) findViewById(R.id.id_btn_lend);
        btnLend.setOnClickListener(this);

        listViewLending.setOnItemClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        updateList();

    }

    @Override
    public void onClick(View v) {
        Intent it = new Intent(this,LendingActivity.class);
        startActivity(it);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent it = new Intent(this,LendingActivity.class);
        it.putExtra("item",itens.get(position));
        startActivity(it);


    }

    public void updateList(){

        itemDAO = new ItemDAO(this);
        itens = itemDAO.getAll();
        itemDAO.closeConnection();

        names = new ArrayList<String>();

        for (Item i: itens) {
            names.add(i.getName());
        }

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,names);
        listViewLending.setAdapter(adapter);

    }


}
