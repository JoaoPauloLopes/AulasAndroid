package com.dotlab.jpls.lending.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.dotlab.jpls.lending.DataBase.PersistenceHelper;
import com.dotlab.jpls.lending.Model.EntityPersistent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JP on 30/04/2017.
 */

public abstract class BasicDAO <T extends EntityPersistent>{

    protected SQLiteDatabase dataBase = null;

    public BasicDAO(Context context) {
        PersistenceHelper persistenceHelper = PersistenceHelper.getInstance(context);
        dataBase = persistenceHelper.getWritableDatabase();
    }

    public abstract String getNameColPrimaryKey();
    public abstract String getNameTable();

    public abstract ContentValues entityToContentValues(T entidade);
    public abstract T contentValuesToEntity(ContentValues contentValues);

    public boolean insert(T entidade) {
        ContentValues values = entityToContentValues(entidade);
        Long result = dataBase.insert(getNameTable(), null, values);

        if(result == -1){

            return false;

        }
        else{

            return true;

        }

    }

    public boolean delete(T t) {

        String[] valoresParaSubstituir = {
                String.valueOf(t.getId())
        };

        int result = dataBase.delete(getNameTable(), getNameColPrimaryKey() + " =  ?", valoresParaSubstituir);

        if(result == 0){

            return false;

        }
        else{

            return true;

        }

    }

    public void deleteAll() {
        dataBase.execSQL("DELETE FROM " + getNameTable());
    }

    public boolean edit(T t) {
        ContentValues valores = entityToContentValues(t);

        String[] valoresParaSubstituir = {
                String.valueOf(t.getId())
        };

        int result = dataBase.update(getNameTable(), valores, getNameColPrimaryKey() + " = ?", valoresParaSubstituir);
        if(result == 0){

            return false;

        }
        else{

            return true;

        }
    }

    public List<T> getAll() {
        String queryReturnAll = "SELECT * FROM " + getNameTable();
        List<T> result = getByQuery(queryReturnAll);

        return result;

    }

    public T getById(int id) {
        String queryOne = "SELECT * FROM " + getNameTable() + " where " + getNameColPrimaryKey() + " = " + id;
        List<T> result = getByQuery(queryOne);
        if(result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public List<T> getByQuery(String query) {

        Cursor cursor = dataBase.rawQuery(query, null);

        List<T> result = new ArrayList<T>();
        if (cursor.moveToFirst()) {
            do {
                ContentValues contentValues = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                T t = contentValuesToEntity(contentValues);
                result.add(t);
            } while (cursor.moveToNext());
        }
        return result;

    }

    public void closeConnection() {
        if(dataBase != null && dataBase.isOpen())
            dataBase.close();
    }

}
