package com.dotlab.jpls.lending.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dotlab.jpls.lending.Model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JP on 30/04/2017.
 */

public class ItemDAO extends BasicDAO<Item>{

    public static final String NAME_TABLE = "Item";
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_DATE_LENDING = "date_lending";
    public static final String COL_DATE_DEVOLUTION = "date_devolution";
    public static final String COL_NAME_PERSON_LENDING = "name_person_lending";
    public static final String COL_CEP = "cep";

    public static final String SCRIPT_CREATE_TABLE_ITEM = "CREATE TABLE " + NAME_TABLE + "("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_NAME + " TEXT,"
            + COL_DATE_LENDING + " TEXT,"
            + COL_DATE_DEVOLUTION + " TEXT,"
            + COL_CEP + " TEXT,"
            + COL_NAME_PERSON_LENDING + " TEXT" + ")";

    private SQLiteDatabase dataBase = null;

    private static ItemDAO instance;


    public static ItemDAO getInstance(Context context) {
        if(instance == null)
            instance = new ItemDAO(context);
        return instance;
    }

    public ItemDAO(Context context) {
        super(context);
    }


    @Override
    public String getNameColPrimaryKey() {
        return COL_ID;
    }

    @Override
    public String getNameTable() {
        return NAME_TABLE;
    }

    @Override
    public ContentValues entityToContentValues(Item item) {

        ContentValues values = new ContentValues();
        if(item.getId() > 0) {
            values.put(COL_ID, item.getId());
        }
        values.put(COL_NAME, item.getName());
        values.put(COL_DATE_LENDING, item.getDateLending());
        values.put(COL_DATE_DEVOLUTION, item.getDateDevolution());
        values.put(COL_NAME_PERSON_LENDING, item.getNamePersonLending());
        values.put(COL_CEP, item.getCep());

        return values;


    }

    @Override
    public Item contentValuesToEntity(ContentValues contentValues) {

        Item item = new Item();
        item.setId(contentValues.getAsInteger(COL_ID));
        item.setName(contentValues.getAsString(COL_NAME));
        item.setDateLending(contentValues.getAsString(COL_DATE_LENDING));
        item.setDateDevolution(contentValues.getAsString(COL_DATE_DEVOLUTION));
        item.setNamePersonLending(contentValues.getAsString(COL_NAME_PERSON_LENDING));
        item.setCep(contentValues.getAsString(COL_CEP));

        return item;

    }


}
