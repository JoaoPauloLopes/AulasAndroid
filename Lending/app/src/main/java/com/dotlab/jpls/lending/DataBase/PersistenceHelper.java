package com.dotlab.jpls.lending.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dotlab.jpls.lending.DAO.ItemDAO;

/**
 * Created by JP on 30/04/2017.
 */

public class PersistenceHelper extends SQLiteOpenHelper {

    public static final String NAME_DB =  "lending_itens";
    public static final int VERSION =  1;

    private static PersistenceHelper instance;

    public static PersistenceHelper getInstance(Context context) {
        if(instance == null)
            instance = new PersistenceHelper(context);

        return instance;
    }

    private PersistenceHelper(Context context) {
        super(context, NAME_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ItemDAO.SCRIPT_CREATE_TABLE_ITEM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
