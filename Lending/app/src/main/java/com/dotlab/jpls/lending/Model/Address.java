package com.dotlab.jpls.lending.Model;

import org.json.JSONObject;

/**
 * Created by JP on 16/05/2017.
 */

public class Address {

    private String cep;
    private String uf;
    private String location;
    private String district;
    private String place;

    public Address(String cep, String uf, String location, String district, String place) {
        this.cep = cep;
        this.uf = uf;
        this.location = location;
        this.district = district;
        this.place = place;
    }

    public Address(JSONObject json) {

        try {

            this.cep = json.getString("cep");
            this.uf = json.getString("uf");
            this.location = json.getString("localidade");
            this.district = json.getString("bairro");
            this.place = json.getString("logradouro");

        }catch (Exception e){


        }

    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public String toString() {
        return   "UF:"+ uf +
                 "\nCEP: "+cep+
                "\nCidade: "+location+
                "\nBairro: " +district+
                "\nLogradouro: "+place;
    }
}
