package com.dotlab.jpls.lending.Model;

/**
 * Created by JP on 30/04/2017.
 */

public interface EntityPersistent {

    public int getId();
    public void setId(int id);


}
