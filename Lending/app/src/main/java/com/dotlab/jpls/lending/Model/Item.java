package com.dotlab.jpls.lending.Model;

import java.io.Serializable;

/**
 * Created by JP on 30/04/2017.
 */

public class Item implements EntityPersistent, Serializable{

    private int id;
    private String name;
    private String dateLending;
    private String dateDevolution;
    private String namePersonLending;
    private String cep;

    public Item (){

    }

    public Item(String name, String dateLending, String dateDevolution, String namePersonLending, String cep) {
        this.name = name;
        this.dateLending = dateLending;
        this.dateDevolution = dateDevolution;
        this.namePersonLending = namePersonLending;
        this.cep = cep;
    }

    public Item(int id, String name, String dateLending, String dateDevolution, String namePersonLending, String cep) {
        this.id = id;
        this.name = name;
        this.dateLending = dateLending;
        this.dateDevolution = dateDevolution;
        this.namePersonLending = namePersonLending;
        this.cep = cep;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateLending() {
        return dateLending;
    }

    public void setDateLending(String dateLending) {
        this.dateLending = dateLending;
    }

    public String getDateDevolution() {
        return dateDevolution;
    }

    public void setDateDevolution(String dateDevolution) {
        this.dateDevolution = dateDevolution;
    }

    public String getNamePersonLending() {
        return namePersonLending;
    }

    public void setNamePersonLending(String namePersonLending) {
        this.namePersonLending = namePersonLending;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        return "Item: "+name+
                "\nEmprestei para: "+namePersonLending+
                "\nData do emprestimo: "+dateLending+
                "\nCEP: "+cep+
                "\nData para devolução: "+dateDevolution;
    }
}
