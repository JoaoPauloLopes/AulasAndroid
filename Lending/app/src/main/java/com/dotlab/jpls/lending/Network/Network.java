package com.dotlab.jpls.lending.Network;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by JP on 16/05/2017.
 */

public class Network {

    private static final int CONNECT_TIMEOUT = 20000;
    private static final int READ_CONNECT = 20000;
    private static final String REQUEST_METHOD = "GET";
    private static final String URL = "https://viacep.com.br/ws/";

    public static String getAddressByCEP(String cep){

        String result = null;

        try{
            URL endPoint = new URL(URL+cep+"/json/");
            HttpURLConnection httpURLConnection;
            InputStream inputStream;

            httpURLConnection = (HttpURLConnection) endPoint.openConnection();
            httpURLConnection.setRequestMethod(REQUEST_METHOD);
            httpURLConnection.setConnectTimeout(CONNECT_TIMEOUT);
            httpURLConnection.setReadTimeout(READ_CONNECT);
            httpURLConnection.connect();

            inputStream = httpURLConnection.getInputStream();

            result = parseToString(inputStream);

        }catch (Exception e)
        {

        }


        return result;

    }


    public static String parseToString(InputStream inputStream){

        StringBuilder stringBuilder = new StringBuilder();
        String row = null;

        try{

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((row = bufferedReader.readLine()) != null){
                stringBuilder.append(row);
            }


        } catch (Exception e)
        {

        }


        return stringBuilder.toString();
    }

}
