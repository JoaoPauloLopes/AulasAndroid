package com.example.uc13201443.listview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/*
*
*  Exemplo ListView e Intent Implícita
*
* */
public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

    private ListView myListView;
    private String[] nomes = {"João","Paulo","lopes","Silva"};
    private Intent it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ListView
        myListView = (ListView) findViewById(R.id.myListView);
        //ArrayAdapter seria tableViewCell no iOS
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,nomes);
        myListView.setAdapter(adapter);
        myListView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Toast.makeText(this,nomes[position],Toast.LENGTH_SHORT).show();
        //Intent implícita não tem contexto
        it = new Intent();
        it.setAction(it.ACTION_SEND);
        it.putExtra(it.EXTRA_TEXT,nomes[position]);
        it.setType("text/plain");
        startActivity(it);


    }
}
