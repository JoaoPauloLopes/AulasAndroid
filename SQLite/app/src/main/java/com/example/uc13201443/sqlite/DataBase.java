package com.example.uc13201443.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by UC13201443 on 19/04/2017.
 */

public class DataBase extends SQLiteOpenHelper{

    private static final String NAME_DB = "rh.db";
    private static final int VERSION = 1;
    public static final String NAME = "name";
    public static final String BIRTH_DATE = "birthDate";
    public static final String NAME_TABLE_PERSON = "Person";


    public DataBase(Context context) {
        super(context,NAME_DB,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlCreateTable = "CREATE TABLE "+ NAME_TABLE_PERSON+"(_id integer primary key autoincrement," +
                                NAME + " text," +
                                BIRTH_DATE + " text)";

        db.execSQL(sqlCreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
