package com.example.uc13201443.sqlite;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ListActivity extends Activity {


    ListView listPerson;

    Person person;
    PersonDAL personDAL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listPerson = (ListView) findViewById(R.id.id_list_person);

        personDAL = new PersonDAL(this);

        Cursor cursor = personDAL.getAllPersons();

        String[] nomeCampos = new String[] {DataBase.NAME, DataBase.BIRTH_DATE};
        int[] idViews = new int[] {R.id.name, R.id.birthDate};

        SimpleCursorAdapter adaptador = new SimpleCursorAdapter(getBaseContext(),
                R.layout.list_view,cursor,nomeCampos,idViews, 0);
        listPerson.setAdapter(adaptador);


    }
}
