package com.example.uc13201443.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by UC13201443 on 19/04/2017.
 */

public class PersonDAL {

    SQLiteDatabase sqLiteDatabase;
    DataBase dataBase;
    ContentValues contentValues;

    public PersonDAL(Context context) {
        this.dataBase = new DataBase(context);
    }

    public String insertPerson(Person person){

        long result;

        contentValues = new ContentValues();
        contentValues.put(DataBase.NAME,person.getName());
        contentValues.put(DataBase.BIRTH_DATE,person.getBirthDate());

        sqLiteDatabase = dataBase.getWritableDatabase();
        result = sqLiteDatabase.insert("Person",null,contentValues);
        sqLiteDatabase.close();
        if(result == -1){

            return "Error";

        }
        else{

            return "Success";

        }


    }

    public Cursor getAllPersons(){

        Cursor cursor;

        sqLiteDatabase = dataBase.getReadableDatabase();

        cursor = sqLiteDatabase.query(DataBase.NAME_TABLE_PERSON,null,null,null,null,null,null);
        if(cursor!=null){
            cursor.moveToFirst();
        }
        sqLiteDatabase.close();
        return cursor;

    }


}
