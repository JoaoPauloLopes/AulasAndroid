package com.example.uc13201443.sqlite;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {

    Person person;
    PersonDAL personDAL;

    Button btnRegister;
    EditText editTextName;
    EditText editTextBirthDate;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        context = this;

        editTextName = (EditText) findViewById(R.id.edit_txt_name);
        editTextBirthDate = (EditText) findViewById(R.id.edit_txt_birth_date);


        register();


    }

    public void register(){

        btnRegister = (Button) findViewById(R.id.btn_register_person);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editTextName.getText().toString().isEmpty() && !editTextBirthDate.getText().toString().isEmpty()) {


                    String result;
                    person = new Person(editTextName.getText().toString(), editTextBirthDate.getText().toString());
                    personDAL = new PersonDAL(context);
                    result = personDAL.insertPerson(person);
                    Toast.makeText(context, result,
                            Toast.LENGTH_LONG).show();
                }
                else{

                    Toast.makeText(context, "Preencha todos os campos",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
