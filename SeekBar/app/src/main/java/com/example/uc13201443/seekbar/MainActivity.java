package com.example.uc13201443.seekbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity  {

    private SeekBar seekbar;
    private TextView textView;
    private SeekBar seekbar2;
    private TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekbar = (SeekBar) findViewById(R.id.idSeekbar);
        textView = (TextView) findViewById(R.id.idTextview);
        textView.setText(String.valueOf(seekbar.getProgress()));

        seekbar2 = (SeekBar) findViewById(R.id.idSeekbar2);
        textView2 = (TextView) findViewById(R.id.idTextview2);
        textView2.setText(String.valueOf(seekbar2.getProgress())+"/"+seekbar2.getMax());

        settingSeekBar();


    }

    private void settingSeekBar(){

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                textView.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                textView2.setText(String.valueOf(progress)+"/"+seekbar2.getMax());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


}
