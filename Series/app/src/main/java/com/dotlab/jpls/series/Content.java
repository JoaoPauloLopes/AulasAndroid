package com.dotlab.jpls.series;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JP on 02/04/2017.
 */

public class Content {


    public static final Serie[] series = {  new Serie("Daredevil",2015,"Action/Drama/Crime",12),
                                            new Serie("Bates Motel",2013,"Drama/Horror/Thriller",18),
                                            new Serie("The Following",2013,"Crime/Drama/Thriller",16),
                                            new Serie("Band of Brothers",2001,"War/Drama/History",18),
                                            new Serie("Mr. Robot ",2015,"Drama/Thriller/Crime",16)
                                        };


    public static final List<String> names (){

        List<String> names = new ArrayList<String>();

        for (Serie s :series) {

            names.add(s.getName());


        }

        return names;
    }


}
