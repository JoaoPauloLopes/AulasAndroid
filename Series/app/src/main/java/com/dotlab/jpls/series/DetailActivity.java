package com.dotlab.jpls.series;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends Activity implements View.OnClickListener{

    private TextView name;
    private TextView year;
    private TextView classification;
    private TextView genre;
    private Button btnShare;
    private Serie serie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        btnShare = (Button) findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);

        name = (TextView) findViewById(R.id.txt_view_name_value);
        year = (TextView) findViewById(R.id.txt_view_value_year);
        classification = (TextView) findViewById(R.id.txt_view_classification_value);
        genre = (TextView) findViewById(R.id.txt_view_genre_value);

        serie = (Serie) getIntent().getExtras().getSerializable("serie");
        if(serie != null){
            name.setText(serie.getName());
            year.setText(Integer.toString(serie.getYear()));
            classification.setText(Integer.toString(serie.getClassification()));
            genre.setText(serie.getGenre());

        }



    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnShare.getId()){

            Intent it = new Intent();
            it.setAction(it.ACTION_SEND);
            it.putExtra(it.EXTRA_TEXT,serie.toString());
            it.setType("text/plain");
            startActivity(it);

        }



    }
}
