package com.dotlab.jpls.series;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener{

    private ListView filmsListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        filmsListView = (ListView) findViewById(R.id.list_view_films);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,Content.names());
        filmsListView.setAdapter(adapter);
        filmsListView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent it = new Intent(this,DetailActivity.class);
        it.putExtra("serie",Content.series[position]);
        startActivity(it);

    }
}
