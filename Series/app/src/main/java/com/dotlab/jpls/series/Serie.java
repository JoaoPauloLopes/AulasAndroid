package com.dotlab.jpls.series;

import java.io.Serializable;

/**
 * Created by JP on 02/04/2017.
 */

public class Serie implements Serializable {

    private String name;
    private int year;
    private String genre;
    private int classification;

    public Serie(String name, int year, String genre, int classification) {
        this.name = name;
        this.year = year;
        this.genre = genre;
        this.classification = classification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getClassification() {
        return classification;
    }

    public void setClassification(int classification) {
        this.classification = classification;
    }

    @Override
    public String toString() {
        return "Nome: " + name + "\n" +
                "Ano: " + year +"\n" +
                "Gênero: " + genre + "\n" +
                "Classificação: " + classification;
    }
}
