package com.example.uc13201443.aswitch;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

public class MainActivity extends Activity  {

    private Switch mySwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mySwitch = (Switch) findViewById(R.id.idSwitch);
        mySwitch.setChecked(true);

        settingSwitch();


    }


    private void settingSwitch(){

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                RelativeLayout lLayout = (RelativeLayout) findViewById(R.id.activity_main);

                if(isChecked){

                    lLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    mySwitch.setTextColor(Color.parseColor("#000000"));


                }
                else{
                    lLayout.setBackgroundColor(Color.parseColor("#000000"));
                    mySwitch.setTextColor(Color.parseColor("#FFFFFF"));

                }

            }
        });


    }
}

