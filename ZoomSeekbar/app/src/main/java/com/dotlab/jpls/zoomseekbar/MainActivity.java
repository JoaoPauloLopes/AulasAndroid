package com.dotlab.jpls.zoomseekbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;

public class MainActivity extends Activity {

    private Switch aSwitchActiveSeekBar;
    private SeekBar seekBarZoomImage;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aSwitchActiveSeekBar = (Switch) findViewById(R.id.id_switch_view_1);
        settingSwitch();

        seekBarZoomImage = (SeekBar) findViewById(R.id.id_seekbar_view_1);
        settingSeekBar();

        imageView = (ImageView) findViewById(R.id.id_image_view_1);



    }

    private void settingSwitch(){

        aSwitchActiveSeekBar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    seekBarZoomImage.setEnabled(false);

                }
                else{

                    seekBarZoomImage.setEnabled(true);

                }

            }
        });

    }

    private void settingSeekBar(){

        seekBarZoomImage.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float scale =  ((progress / 10.0f)+1);
                imageView.setScaleX(scale);
                imageView.setScaleY(scale);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
